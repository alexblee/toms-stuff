import random
from zPlayer import Player

inv = []

class NPC:

    def __init__(self,name,description,location,greeting,hp,dead_description,max_damage,friendly,loot,ldc):
        self.name = name
        self.description = description        
        self.location = location
        self.greeting = greeting
        self.hp = hp
        self.dead_description = dead_description
        self.max_damage = max_damage
        self.friendly = friendly
        self.loot = loot
        self.ldc = ldc
     

    # This function is called when we attack the player
    # The return value is how much damage we caused.
    def attack(self):
        npc_attack_damage = random.randint(0,self.max_damage)
        print(self.name + " attack does " + str(npc_attack_damage) + " damage")
        return npc_attack_damage

    def guardedAttack(self):
        npc_guard_damage = random.randint(0,self.max_damage/2)
        print("You blocked and " + self.name + "'s" + " attack did " + (str(round(npc_guard_damage, 1))) + " damage")
        return npc_guard_damage

    def isDead(self): 
        if(self.hp < 1):
            return True
        else:
            return False

    def dropItem(self):
        npcdroprate = int(self.ldc)
        droprate = random.randint(1,npcdroprate)
        if(droprate == npcdroprate):                       
            print("You have obtained " + self.loot + "!")

    def render(self):
        print(self.name)
        if (self.isDead() == False):           
            print(self.description)
            print(self.name + " says " + self.greeting)
            if(self.friendly == 0):
                print("[ attack ] this creature wants to fight [" + str(self.hp) + "]")
                print("[ guard ] block an attack")
                print("[ heal ] heal yourself.")
                
        else:
            print(self.dead_description)

            
            
            

