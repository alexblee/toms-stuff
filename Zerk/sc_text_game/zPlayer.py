import random

class Player:

    def __init__(self,currentPlaceName,hp,min_damage,max_damage,inv):
        self.currentPlaceName = "SchoolCreative"
        self.hp = hp
        self.min_damage = min_damage
        self.max_damage = max_damage
        self.inv = []
        
    

    def isDead(self):
    	if(self.hp < 1):
    		return True
    	else:
    		return False

    def attack(self):
        player_attack_damage = random.randint(self.min_damage,self.max_damage)               
        print("Player attack does " + str(player_attack_damage) + " damage")
        return player_attack_damage


    def crit(self):
        crit = random.randint(1,3)
        if(crit == 3):
            player_attack_damageC = random.randint(self.min_damage*2,self.max_damage*2)
            print("Critical Hit!")
            print("Player attack does " + str(player_attack_damageC) + " damage")
            return player_attack_damageC

    def heal(self):
        hPer = random.randint(1,2)             
        healAmount = 20
        if(hPer == 2):
            if(self.hp == 100):
                 print("You are already at full health!")
            else:
                print("You have been healed. Your health is now at " + str(self.hp) + " HP")
                self.hp += healAmount
                if(self.hp >= 100):
                    self.hp = 100
        else:
            print("Heal failed!")

    def modAttack(self):
        if('Excalibur' in self.inv):
            self.min_damage = 2147483647
            self.max_damage = 2147483647
        elif('Diploma' in self.inv):
            self.min_damage = 1000
            self.max_damage = 2000
        elif('Coffee' in self.inv):
            self.min_damage = 300
            self.max_damage = 600
        elif('Monopoly Board' in self.inv):
            self.min_damage = 50
            self.max_damage = 100
        elif('Notebook' in self.inv):
            self.min_damage = 25
            self.max_damage = 50

    def hpCheck(self):
        print("You have " + str(self.hp) + " HP!")

    def invCheck(self):
        print(self.inv)







