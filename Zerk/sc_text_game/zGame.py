import csv
import random
from zPlayer import Player
from zPath import Path
from zPlace import Place
from zNPC import NPC

class Game:

    def __init__(self):
        # stores all the places in the game
        self.places = {}
        self.paths =  {}
        self.npcs = {}
        self.player = Player("SchoolCreative", 100, 1, 10, [])

# -------------------------------------- PLACE, EXIT, NPC LOADING ----------------------------------
   
    def load_places(self):   
        filename = "Places.csv"
        file = open(filename)
        reader = csv.reader(file)   # object containing the rows

        for row in reader:          # each row is a list
            name = row[0]
            description = row[1]
            place_exits = []
            npcs = []
            rowLength = len(row)
            place = Place(name, description, place_exits)
            self.places[name] = place
            numberOfExits = rowLength -2
            exitIndex = 2

            while exitIndex < rowLength:
                exitName = row[exitIndex]
                exitPath = self.paths[exitName]
                place_exits.append(exitPath)
                exitIndex += 1

    def load_paths(self):
        filename = "Exits.csv"
        file = open(filename)
        reader = csv.reader(file)        
   
        for row in reader:
            # row = name, description
            name = row[0]
            description = row[1]
            keyCommandString = row[2]
            destination = row[3]
            rowLength = len(row)
            path = Path(description, keyCommandString, destination)
            self.paths[name] = path


    def load_npcs(self):
            filename = "NPC.csv"
            file = open(filename)
            reader = csv.reader(file)        
       
            for row in reader:
                # row = name, description
                name = row[0]
                description = row[1]
                location = row[2]
                greeting = row[3]
                hp = int(row[4])
                dead_description = row[5]
                max_damage = int(row[6])
                friendly = bool(int(row[7]))
                loot = row[8]
                ldc = row[9]
                npcs = []
                rowLength=len(row)
                npc = NPC(name,description,location,greeting,hp,dead_description,max_damage,friendly,loot,ldc) # CREATES AN NPC OUTPUT
                self.npcs[name] = npc 


# -------------------------------All the other things that should run the game -----------------------------------------------
    def run(self):

        while(1):
            P = self.places[self.player.currentPlaceName]
            P.render()

            room_person = None

            for person_name in self.npcs:
                person = self.npcs[person_name] 
                if (person.location == P.name):
                    room_person = person


            if(room_person):
                room_person.render()
                           
            player_action = input("-->")

            if(player_action == "attack"):
                
                if(room_person == None):
                    print("There is nobody here to attack!!!!!!")

                elif(room_person.friendly == True):
                    print(room_person.name + " is friendly. You can't attack them!")

                elif(room_person.isDead()):
                    print("You attack the dead body. You're gonna be cursed by the dead.")
                
                elif(room_person):
                    
                    self.player.hp -= room_person.attack()
                    room_person.hp -= self.player.attack()

            if(player_action == "guard"):

                if(room_person.isDead()):
                	print("There's nothing to guard yourself from!")
                else:
	                self.player.hp -= round(room_person.guardedAttack())

            if(player_action == "heal"):
                
                if(room_person == None):
                	self.player.heal()

                if(room_person):                   
                    if(room_person.isDead()):
                            self.player.heal()
                    
                    else:    
                        self.player.hp -= room_person.attack()
                        self.player.heal()
            

            if(player_action == "inv"):
                self.player.invCheck()

            if(player_action == "hp"):
            	self.player.hpCheck()


            if(player_action == "pick up notebook"):
                if('Notebook' in P.floor):
                    P.floor.remove('Notebook')
                    self.player.inv.append('Notebook')
                    print("You pick up a notebook.")
                else:
                    print("You try to pick up said object.. but it's not even there.")

            # if(player_action == "pick up excalibur"):
            #     if('Excalibur' in P.floor):
            #         P.floor.remove('excalibur')
            #         self.player.inv.append('excalibur')
            #         print("You pick up a excalibur.")
            #     else:
            #         print("You try to pick up said object.. but it's not even there.")

            # if(player_action == "pick up diploma"):
            #     if('Diploma' in P.floor):
            #         P.floor.remove('diploma')
            #         self.player.inv.append('diploma')
            #         print("You pick up a diploma.")
            #     else:
            #         print("You try to pick up said object.. but it's not even there.")

            # if(player_action == "pick up coffee"):
            #     if('Coffee' in P.floor):
            #         P.floor.remove('coffee')
            #         self.player.inv.append('coffee')
            #         print("You pick up a coffee.")
            #     else:
            #         print("You try to pick up said object.. but it's not even there.")

            # if(player_action == "pick up monopoly board"):
            #     if('Monopoly Board' in P.floor):
            #         P.floor.remove('monopoly board')
            #         self.player.inv.append('monopoly board')
            #         print("You pick up a monopoly board.")
            #     else:
            #         print("You try to pick up said object.. but it's not even there.")

            elif(room_person.isDead()):
                room_person.dropItem()
                self.player.inv.append(room_person.loot)
                self.player.modAttack()
            else:
                print("There's nothing to pick up!")

            if(player_action == "kill self"):
            	print("You decide to kill yourself. Why?")
            	self.player.hp = 0

            if(player_action == "drop notebook"):
                if('Notebook' in self.player.inv):
                    self.player.inv.remove('Notebook')
                    P.floor.append('Notebook')
                    print("You dropped notebook!")
                else:
                    print("You don't have this item!")
                


            if(self.player.isDead()):
                print("You have been killed!")
                exit(0)


            for E in P.exits:
                if ( E.keyCommand == player_action):                  
                    self.player.currentPlaceName = E.destination

