import pygame
import sys
import random
from math import *
from pygame.locals import *

pygame.init()

display_width = 480
display_height = 600
clock = pygame.time.Clock()

gameDisplay = pygame.display.set_mode((display_width,display_height))
pygame.display.set_caption("SHOOP")

default_colour = [0, 0 ,0]
blue_colour = [ 30, 144, 255 ]
red_colour = [ 255, 0 , 0 ]
red = [200,0,0]
green = [0,200,0]
green_colour = [0,255,0]
white = [255,255,255]

text_file = open("Highscores.txt", "w")

score_list = []




background = pygame.image.load("space.jpg")
background_rect = background.get_rect()

player_img = pygame.image.load("playerShip1_red.png")

meteor_img = pygame.image.load("meteorBrown_med1.png")

plosion = pygame.image.load("plosion.png")
plosion.set_colorkey(default_colour)

shoot_sound = pygame.mixer.Sound("pew.wav")
expl_sounds = []

pygame.mixer.music.load("FrozLoop.mp3")
pygame.mixer.music.set_volume(0.4)
pygame.mixer.music.play(-1)

for snd in ['expl3.wav', 'expl6.wav']:
	expl_sounds.append(pygame.mixer.Sound(snd))

def newmob():
	m = Mob()
	all_sprites.add(m)
	mobs.add(m)

def draw_text(surf,text,size,x,y):
    largeText = pygame.font.Font('freesansbold.ttf',25)
    text_surface = largeText.render(text, True, white)
    text_rect = text_surface.get_rect()
    text_rect.midtop = (x, y)
    surf.blit(text_surface, text_rect)

def show_gameover_screen():
    gameDisplay.blit(background, background_rect)
    draw_text(gameDisplay, "GAME OVER!", 64, display_width / 2, display_height / 4)
    draw_text(gameDisplay, "Your score was: " + str(score), 22,
              display_width / 2, display_height / 2)
    draw_text(gameDisplay, "Press space to try again.", 18, display_width / 2, display_height * 3 / 4)
    pygame.display.flip()
    with open('Highscores.txt', 'w') as f:
        score_list.append(score)
        score_list.sort(reverse = True)
        f.write("Scores: \n" + str(score_list))
        print(score_list)
    waiting = True
    while waiting:
        clock.tick(60)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                text_file.close()
                pygame.quit()
            if event.type == pygame.KEYDOWN:
            	if event.key == K_SPACE:
                    waiting = False
            	if event.key == pygame.K_ESCAPE:
                    sys.exit()


def show_menu_screen():
    gameDisplay.blit(background, background_rect)
    draw_text(gameDisplay, "SHMOOP", 64, display_width / 2, display_height / 4)
    draw_text(gameDisplay, "Arrow keys move, Space to fire", 22,
              display_width / 2, display_height / 2)
    draw_text(gameDisplay, "Press space to begin", 18, display_width / 2, display_height * 3 / 4)
    pygame.display.flip()
    waiting = True
    while waiting:
        clock.tick(60)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                text_file.close()
                pygame.quit()
            if event.type == pygame.KEYDOWN:
            	if event.key == K_SPACE:
                	waiting = False
            	if event.key == pygame.K_ESCAPE:
            		sys.exit()

            		 	



class Player(pygame.sprite.Sprite):
	def __init__(self):
		pygame.sprite.Sprite.__init__(self)
		self.image = player_img
		self.image = pygame.transform.scale(player_img, (50,38))
		self.rect = self.image.get_rect()
		self.radius = int(self.rect.width * .75/2)
		self.rect.centerx = display_width/2
		self.rect.centery = display_height-20

		self.hspeed = 0
		self.vspeed = 0

	def update(self):
		self.hspeed = 0
		self.vspeed = 0
		keystate = pygame.key.get_pressed()
		if keystate[pygame.K_LEFT]:
			self.hspeed = -5
		if keystate[pygame.K_RIGHT]:
			self.hspeed = 5
		if keystate[pygame.K_UP]:
			self.vspeed = -5
		if keystate[pygame.K_DOWN]:
			self.vspeed = 5
		self.rect.x += self.hspeed 
		self.rect.y += self.vspeed

		if self.rect.right > display_width:
			self.rect.right = display_width

		if self.rect.left < 0:
			self.rect.left = 0

		if self.rect.bottom > display_height:
			self.rect.bottom = display_height

		if self.rect.top < 0:
			self.rect.top = 0 

	def shoot(self):
		bullet = Bullet(self.rect.centerx, self.rect.top)
		all_sprites.add(bullet)
		bullets.add(bullet)
		shoot_sound.play()

class Mob(pygame.sprite.Sprite):
	def __init__(self):
		pygame.sprite.Sprite.__init__(self)
		self.image = meteor_img
		self.rect = self.image.get_rect()
		self.radius = int(self.rect.width/2)
		self.rect.centerx = random.randrange(display_width - self.rect.width)
		self.rect.centery = random.randrange(-100,0)
		self.speedy = random.randrange(4,8)
		self.speedx = random.randrange(-6,6)

	def update(self):
		self.rect.centery += self.speedy
		self.rect.centerx += self.speedx
		if self.rect.top > display_height + 10 or self.rect.left < -25 or self.rect.right > display_width + 20:
			self.rect.centerx = random.randrange(display_width - self.rect.width)
			self.rect.centery = random.randrange(-100,50)
			self.speedy = random.randrange(1,8)


class Bullet(pygame.sprite.Sprite):
	def __init__(self,x,y):
		pygame.sprite.Sprite.__init__(self)
		self.image = pygame.Surface((10,20))
		self.image.fill(blue_colour)
		self.rect = self.image.get_rect()
		self.rect.bottom = y
		self.rect.centerx = x
		self.speedy = -10

	def update(self):
		self.rect.y += self.speedy
		if self.rect.bottom < 0:
			self.kill()


pygame.time.set_timer(USEREVENT + 1, 100)


game_over = False
menu = True
ting = True
score = 0

while(ting == True):
	clock.tick(60)
	if menu:
		
		spawn_amount = 40
		show_menu_screen()
		menu = False
		all_sprites = pygame.sprite.Group()
		mobs = pygame.sprite.Group()
		bullets = pygame.sprite.Group()
		powerups = pygame.sprite.Group()
		player = Player()
		all_sprites.add(player)
		for i in range(spawn_amount):
		    newmob()
		score = 0

	if game_over:
		spawn_amount = 40
		show_gameover_screen()
		game_over = False
		all_sprites = pygame.sprite.Group()
		mobs = pygame.sprite.Group()
		bullets = pygame.sprite.Group()
		powerups = pygame.sprite.Group()
		player = Player()
		all_sprites.add(player)
		for i in range(spawn_amount):
		    newmob()
		score = 0

	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			ting = False
		if event.type == USEREVENT + 1:
		    score += 1
		if event.type == pygame.KEYDOWN:
			if event.key == pygame.K_ESCAPE:
				sys.exit()
				text_file.close()



	all_sprites.update()
	hits = pygame.sprite.spritecollide(player, mobs, False, pygame.sprite.collide_circle)
	if hits:
		game_over = True

  



	hits = pygame.sprite.groupcollide(mobs, bullets, True, True)
	for hit in hits:
		random.choice(expl_sounds).play()
		m = Mob()
		all_sprites.add(m)
		mobs.add(m)

	gameDisplay.fill(default_colour)
	gameDisplay.blit(background,background_rect)
	all_sprites.draw(gameDisplay)
	draw_text(gameDisplay, str(score), 18, display_width/2, 10)

	pygame.display.update()


pygame.quit()
quit()
