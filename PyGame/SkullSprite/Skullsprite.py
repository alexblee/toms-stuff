import pygame
import sys

pygame.init()

display_width = 800
display_height = 600

default_colour = [0, 0 ,0]
blue_colour = [ 30, 144, 255 ]
red_colour = [ 255, 0 , 0 ]
green_colour = [0,255,0]
yellow_colour = [255,255,0]


gameDisplay = pygame.display.set_mode((display_width,display_height))
pygame.display.set_caption("Slither")

collision = pygame.mixer.Sound("collision.wav")
yeahboi = pygame.mixer.Sound("yeahboi.wav")
spooooky = pygame.mixer.music.load("spooooky.mp3")
pygame.mixer.music.play(-1)	

class Skull(pygame.sprite.Sprite):
	def __init__(self,color = blue_colour, width = 64, height = 64):
		super(Skull,self).__init__()
		self.image = pygame.Surface((width,height))
		self.image.fill(color)
		self.set_properties()

		self.hspeed = 0
		self.vspeed = 0

	def set_properties(self):
		self.rect = self.image.get_rect()
		self.origin_x = self.rect.centerx
		self.origin_y = self.rect.centery

	def change_speed(self,hspeed,vspeed):
		self.hspeed += hspeed
		self.vspeed += vspeed
		
	def set_position(self,x,y):
		self.rect.x = x - self.origin_x
		self.rect.y = y - self.origin_y

	def set_image(self,filename = None):
		if(filename != None):
			self.image = pygame.image.load(filename)
			self.set_properties()

	def update(self):
		self.rect.x += self.hspeed
		self.rect.y += self.vspeed

skull_group = pygame.sprite.Group()

a_skull = Skull()
a_skull.set_image("thing.png")
a_skull.set_position(0, 0)

skull_group.add(a_skull)



clock = pygame.time.Clock()
ting = True

while(ting == True):

	for event in pygame.event.get():

		if event.type == pygame.KEYDOWN:
			
			if event.key == pygame.K_ESCAPE:
				sys.exit()

			if event.key == pygame.K_LEFT:
				a_skull.change_speed(-5,0)

			if event.key == pygame.K_RIGHT:
				a_skull.change_speed(5,0)

			if event.key == pygame.K_UP:
				a_skull.change_speed(0,-5)

			if event.key == pygame.K_DOWN:
				a_skull.change_speed(0,5)

			if event.key == pygame.K_SPACE:
				pygame.mixer.Sound.play(yeahboi)

		if event.type == pygame.KEYUP:

			if event.key == pygame.K_LEFT:
				a_skull.change_speed(5,0)

			if event.key == pygame.K_RIGHT:
				a_skull.change_speed(-5,0)

			if event.key == pygame.K_UP:
				a_skull.change_speed(0,5)

			if event.key == pygame.K_DOWN:
				a_skull.change_speed(0,-5)

			if event.key == pygame.K_SPACE:
				pygame.mixer.Sound.play(yeahboi)

		# if(event.type == pygame.MOUSEMOTION):
		# 	mouse_pos = pygame.mouse.get_pos()
		# 	a_skull.set_position(mouse_pos[0],mouse_pos[1])

	# if(a_skull.rect.x>wallXmax):
	# 	a_skull.rect.x=wallXmax
	# 	a_skull.change_speed(0,0)
	# 	print("first")
	# 	pygame.mixer.Sound.play(collision)

	# if(a_skull.rect.x<wallXmin):
	# 	a_skull.rect.x=wallXmin
	# 	a_skull.change_speed(0,0)
	# 	print("second")
	# 	pygame.mixer.Sound.play(collision)

	# if(a_skull.rect.y>wallYmax):
	# 	a_skull.rect.y=wallYmax
	# 	a_skull.change_speed(0,0)
	# 	print("third")
	# 	pygame.mixer.Sound.play(collision)

	# if(a_skull.rect.y<wallYmin):
	# 	a_skull.rect.y=wallYmin
	# 	a_skull.change_speed(0,0)
	# 	print("last")
	# 	pygame.mixer.Sound.play(collision)



	a_skull.update()
	print(a_skull.hspeed,a_skull.vspeed)
	clock.tick(60)

	gameDisplay.fill(blue_colour)
	skull_group.draw(gameDisplay)
	pygame.display.update()



pygame.quit()
quit()
