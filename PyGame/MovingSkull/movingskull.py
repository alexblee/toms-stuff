import pygame
import sys

pygame.init()

display_width = 800
display_height = 600

default_colour = [0, 0 ,0]
blue_colour = [ 30, 144, 255 ]
red_colour = [ 255, 0 , 0 ]
green_colour = [0,255,0]
yellow_colour = [255,255,0]


gameDisplay = pygame.display.set_mode((display_width,display_height))
pygame.display.set_caption("POWERUP SKULL!")


collision = pygame.mixer.Sound("collision.wav")
yeahboi = pygame.mixer.Sound("yeahboi.wav")
spooooky = pygame.mixer.music.load("spooooky.mp3")
pygame.mixer.music.play(-1)	


a_skull = Skull("thing.png")
skull_group = pygame.sprite.Group()
skull_group.add(a_skull)

imgWidth = a_skull.get_rect().size[0]
imgHeight = a_skull.get_rect().size[1]

img_X_min = 0
img_X_max = display_width - imgWidth

img_Y_min = 0
img_Y_max = display_height - imgHeight


x = (display_width * 0.5) - (imgWidth * 0.5)
y = (display_height * 0.5) - (imgHeight * 0.5)

x_change = 0
y_change = 0

wallXmin = 0
wallYmin = 0
wallXmax = display_width - imgWidth
wallYmax = display_height - imgHeight


def drawSkull(x,y):
	skull_group.draw(x,y)


ting = True

while(ting == True):

	skullGroup.update()

	for event in pygame.event.get():
		keys = pygame.key.get_pressed()
		if event.type == pygame.KEYDOWN:
			
			if event.key == pygame.K_ESCAPE:
				sys.exit()

			if event.key == pygame.K_LEFT:
				x_change = -0.5
				y_change = 0

			if event.key == pygame.K_RIGHT:
				x_change = 0.5
				y_change = 0

			if event.key == pygame.K_UP:
				y_change = -0.5
				x_change = 0

			if event.key == pygame.K_DOWN:
				y_change = 0.5
				x_change = 0

			if event.key == pygame.K_SPACE:
				pygame.mixer.Sound.play(yeahboi)

			if keys[pygame.K_RIGHT] and keys[pygame.K_UP]:
				x_change = 0.5
				y_change = -0.5

			if keys[pygame.K_RIGHT] and keys[pygame.K_DOWN]:
				x_change = 0.5
				y_change = 0.5

			if keys[pygame.K_LEFT] and keys[pygame.K_UP]:
				x_change = -0.5
				y_change = -0.5

			if keys[pygame.K_LEFT] and keys[pygame.K_DOWN]:
				x_change = -0.5
				y_change = 0.5


	if(x>wallXmax):
		x=wallXmax
		x_change = 0
		print("first")
		pygame.mixer.Sound.play(collision)

	if(x<wallXmin):
		x=wallXmin
		x_change = 0
		print("second")
		pygame.mixer.Sound.play(collision)

	if(y>wallYmax):
		y=wallYmax
		y_change = 0
		print("third")
		pygame.mixer.Sound.play(collision)

	if(y<wallYmin):
		y=wallYmin
		y_change = 0
		print("last")
		pygame.mixer.Sound.play(collision)

	x+=x_change
	y+=y_change

	print(x,x_change)
	print(y,y_change)

	gameDisplay.fill(blue_colour)
	skullGroup.draw(gameDisplay)
	pygame.display.update()



pygame.quit()
quit()

